const express = require('express')
const router = express.Router()


const { home, form, rankAdd } = require('../controllers/rank')
const { addItem, formItem, delItem, incrementStars, decrementStars } = require('../controllers/item')

// rank
router.get('/', home)
router.get('/form', form)
router.post('/add', rankAdd)

//item
router.post('/addItem/:rankId', addItem)
router.get('/formItem', formItem)
router.get('/delItem/:id', delItem)
router.get('/incStars/:id', incrementStars)
router.get('/dncStars/:id', decrementStars)


module.exports = router