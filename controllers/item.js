const {Types} = require('mongoose')
const Item = require('../models/item')
const Rank = require('../models/rank')


const addItem = async(req, res) => {
   console.log(req.body)
    const item = await Item({
        _id: new Types.ObjectId(),
        name: req.body.item,
        numStars: req.body.numStars
    })
    await Rank.updateOne({_id: req.params.rankId}, {$push: {itens: item}})
    item.save()
    res.redirect('back')
}

const formItem = (req, res) => {
    res.render('formItem')
}

const delItem = async(req, res) => {
    await Rank.updateOne({_id: req.params.id}, {$pull: { itens: req.params.id}})       
    await Item.deleteOne({_id: req.params.id})
    res.redirect('back')
}

const incrementStars = async(req, res) => {
    await Item.updateOne({_id: req.params.id}, {$inc: {numStars: 1}})
    res.redirect('back')
}

const decrementStars = async(req, res) => {
    await Item.updateOne({_id: req.params.id}, {$inc: {numStars: -1}})
    res.redirect('back')
}

module.exports = {
    addItem,
    formItem,
    delItem,
    incrementStars,
    decrementStars

}