const {Types} = require('mongoose')

const Rank = require('../models/rank')
const Item = require('../models/item')


const home = async(req, res) => {
    const rank = await Rank.find({}).populate({ 
        path:"itens",
        options: {
            sort:'-numStars'
        }
    })
    const itens = await Item.find({})
    const rankItens = {
        rank,
        itens
    }
    res.render('index', {
        ranks: rank,
        rankItens
    })
}

const form = async(req, res) => {
    const item = await Item.find({})
    res.render('form',{item})
}

const rankAdd = async(req, res) => {
    const rank = await Rank({
        _id: Types.ObjectId(), 
        title: req.body.title
    })

    rank.save()
    res.redirect('/')
}

module.exports = {
    home,
    form,
    rankAdd
}