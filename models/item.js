const { Schema, model } = require('mongoose')


const itemSchema = ({
    name: {type: String, required: true},
    numStars: {type: Number}
})


module.exports = model('Item', itemSchema)