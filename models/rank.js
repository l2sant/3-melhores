const { Schema, model } = require('mongoose')

const rankSchema = Schema({
    _id: Schema.Types.ObjectId,
    title: {type: String, required: true},
    itens: [{type:Schema.Types.ObjectId, ref: "Item"}]
}) 


module.exports = model('Rank', rankSchema)